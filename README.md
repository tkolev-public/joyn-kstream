**Requirements** 

Download the Confluent Platform Quick Start and set it up
Create three topics: users, pageviews and top_pages
Use the Kafka Connect DataGenConnector to produce data into these topics, using the users_extended and pageview quickstarts
Use a stream processing engine other than KSQL to develop a processor that:

- Joins the messages in these two topics on the user id field
- Uses a 1 minute hopping window with 10 second advances to compute the 10 most viewed pages by viewtime for every value of gender
- Once per minute produces a message into the top_pages topic that contains the gender, page id, sum of view time in the latest window and distinct count of user ids in the latest window


**How to run**

To assemble the jar `sbt clean assembly`.

Then to run it `java -cp target/scala-2.12/joyn-kstream-assembly-0.1.jar Main`

The app expects the input topic names to be `users_extended` and `pageviews` and the data to be in avro format. For the latter, please pass the following flag to the datagen tool `value-format=avro`

Commands I've used to run data through the app:

`bin/ksql-datagen quickstart=users_ topic=users_extended key-format=kafka value-format=avro`

`bin/ksql-datagen quickstart=pageviews topic=pageviews key-format=kafka value-format=avro`
