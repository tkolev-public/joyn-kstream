import java.time.Duration
import java.util.{Collections, Properties}

import com.sksamuel.avro4s.BinaryFormat
import com.sksamuel.avro4s.kafka.GenericSerde
import io.confluent.kafka.streams.serdes.avro.GenericAvroSerde
import org.apache.kafka.streams.{KafkaStreams, StreamsConfig}
import model._

object Main extends App {

  val WindowDuration = Duration.ofMinutes(1)
  val WindowAdvanceBy = Duration.ofSeconds(10)

  val PageViewTopicName = "pageviews"
  val UsersTopicName = "users_extended"
  val OutputTopicName = "top_pages"
  val WindowStoreName = "top_pages_store"

  val config: Properties = {
    val props = new Properties()
    props.put(StreamsConfig.APPLICATION_ID_CONFIG, "joyn-pageview-count")
    props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092")
    props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092")
    props.put(StreamsConfig.COMMIT_INTERVAL_MS_CONFIG, "60000")
    props
  }

  val serdeConfig =
    Collections.singletonMap("schema.registry.url", "http://localhost:8081")
  val userSerde: GenericSerde[User] = new GenericSerde[User](BinaryFormat)
  val pageViewSerde: GenericSerde[PageView] =
    new GenericSerde[PageView](BinaryFormat)
  val genericAvroSerde = new GenericAvroSerde()
  genericAvroSerde.configure(serdeConfig, false)
  userSerde.configure(serdeConfig, false)
  pageViewSerde.configure(serdeConfig, false)

  val topology =
    PageViewsApp(WindowDuration, WindowAdvanceBy)
      .createTopology(PageViewTopicName,
                      UsersTopicName,
                      OutputTopicName,
                      WindowStoreName)(
        userSerde,
        pageViewSerde,
        genericAvroSerde)

  val streams: KafkaStreams = new KafkaStreams(topology, config)
  streams.start()
  sys.ShutdownHookThread {
    streams.close(Duration.ofSeconds(10))
  }
}
