import com.sksamuel.avro4s.BinaryFormat
import com.sksamuel.avro4s.kafka.GenericSerde
import model.{Aggregate, AggregateKey, PageViewResult, TopPage}

object AppSerdes {

  implicit val pageViewResultSerde: GenericSerde[PageViewResult] =
    new GenericSerde[PageViewResult](BinaryFormat)

  implicit val topTenSerde: GenericSerde[Aggregate] =
    new GenericSerde[Aggregate](BinaryFormat)

  implicit val topPageSerde: GenericSerde[TopPage] =
    new GenericSerde[TopPage](BinaryFormat)
}
