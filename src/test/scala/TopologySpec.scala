import java.time.{Duration, Instant}
import java.util.Properties

import org.apache.kafka.common.serialization.{
  LongSerializer,
  StringDeserializer,
  StringSerializer
}
import org.apache.kafka.streams.scala.Serdes
import org.apache.kafka.streams.{StreamsConfig, TopologyTestDriver}
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

import model._
import TestUtils._

class TopologySpec extends AnyFlatSpec with Matchers {

  val config = {
    val props = new Properties()
    props.put(StreamsConfig.APPLICATION_ID_CONFIG, "pageview-count")
    props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092")
    props.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG,
              Serdes.String.getClass.getName)
    props.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG,
              Serdes.String.getClass.getName)
    props.put("schema.registry.url", MockSchemaRegistryUrl)
    props
  }

  val topology = PageViewsApp(Duration.ofSeconds(5), Duration.ofSeconds(1))

  it should "accumulate top ten pages correctly" in {
    val topTenSoFar = Map(
      "page1" -> 1L,
      "page2" -> 2L,
      "page3" -> 3L,
      "page4" -> 4L,
      "page5" -> 5L,
      "page6" -> 6L,
      "page7" -> 7L,
      "page8" -> 8L,
      "page9" -> 9L,
      "page10" -> 10L,
    )

    val initAgg = Aggregate(Set.empty[String], topTenSoFar, 0, "MALE")
    val topTen1 = PageViewsApp
      .aggregateData("key",
                     PageViewResult(1000, "userid1", "page11", "FEMALE"),
                     initAgg)
      .topTenPages
    topTen1.size shouldBe 10
    topTen1.contains("page1") || topTen1.contains("page11") shouldBe true
    topTen1.contains("page1") && topTen1.contains("page11") shouldBe false

    val topTen2 = PageViewsApp
      .aggregateData("key",
                     PageViewResult(1000, "userid1", "page11", "FEMALE"),
                     initAgg.copy(topTenPages = topTen1))
      .topTenPages
    topTen2.size shouldBe 10
    topTen2.contains("page11") shouldBe true
    topTen2.contains("page1") shouldBe false
  }

  it should "aggregate page views per window" in {

    val app = PageViewsApp(Duration.ofSeconds(5), Duration.ofSeconds(1))
    val usersTopicName = "users"
    val pageviewsTopicName = "pageviews"
    val outputTopicName = "output-topic"
    val storeName = "pageview-counts"
    val topology = app.createTopology(pageviewsTopicName,
                                      usersTopicName,
                                      outputTopicName,
                                      storeName)
    val driver = new TopologyTestDriver(topology, config)

    val pageViewsInputTopic =
      driver.createInputTopic(pageviewsTopicName,
                              new LongSerializer(),
                              testAvroSerde.serializer())
    val usersInputTopic = driver.createInputTopic(usersTopicName,
                                                  new StringSerializer(),
                                                  testAvroSerde.serializer())
    val outputTopic = driver.createOutputTopic(outputTopicName,
                                               new StringDeserializer(),
                                               topPageSerde.deserializer())

    usersInputTopic.pipeInput("userid1", User("userid1", "MALE"))
    usersInputTopic.pipeInput("userid2", User("userid2", "MALE"))
    usersInputTopic.pipeInput("userid2", User(null, "MALE")) //will filter out beacuse userid == null
    usersInputTopic.pipeInput("userid3", User("userid3", "FEMALE"))
    val now = Instant.now()
    val event1Ts = now.plusMillis(0)
    pageViewsInputTopic.pipeInput(1000L,
                                  PageView(1000L, "userid1", "pageid1"),
                                  event1Ts)
    pageViewsInputTopic.pipeInput(1000L,
                                  PageView(1000L, "userid1", "pageid2"),
                                  event1Ts.plusSeconds(1))
    pageViewsInputTopic.pipeInput(1000L,
                                  PageView(1000L, "userid2", "pageid2"),
                                  event1Ts.plusSeconds(1))
    pageViewsInputTopic.pipeInput(1000L,
                                  PageView(1000L, "userid3", "pageid2"),
                                  event1Ts.plusSeconds(2))
    pageViewsInputTopic.pipeInput(1000L,
                                  PageView(1000L, null, "pageid2"),
                                  event1Ts.plusSeconds(2)) //will filter out because userid == null
    pageViewsInputTopic.pipeInput(1000L,
                                  PageView(1000L, "userid1", "pageid1"),
                                  event1Ts.plusSeconds(4))
    outputTopic.readValue() shouldBe TopPage("pageid1", "MALE", 1000, 1)
    outputTopic.readValue() shouldBe TopPage("pageid1", "MALE", 3000, 2)
    outputTopic.readValue() shouldBe TopPage("pageid2", "MALE", 3000, 2)
    outputTopic.readValue() shouldBe TopPage("pageid2", "FEMALE", 1000, 1)
    outputTopic.readValue() shouldBe TopPage("pageid1", "MALE", 3000, 2)
    outputTopic.readValue() shouldBe TopPage("pageid2", "MALE", 3000, 2)
    outputTopic.readValue() shouldBe TopPage("pageid2", "FEMALE", 1000, 1)
    outputTopic.readValue() shouldBe TopPage("pageid1", "MALE", 3000, 2)
    outputTopic.readValue() shouldBe TopPage("pageid2", "MALE", 3000, 2)
    driver.close()
  }

}
