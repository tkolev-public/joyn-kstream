
name := "joyn-kstream"

version := "0.1"

scalaVersion := "2.12.11"

resolvers += "confluent" at "http://packages.confluent.io/maven/"

libraryDependencies ++= Seq(
  "org.apache.kafka" %% "kafka-streams-scala" % "2.5.0",
//  "org.apache.kafka" %% "kafka-streams-avro-serde" % "2.5.0",
  "io.confluent" % "kafka-streams-avro-serde" % "5.5.0",
  "com.sksamuel.avro4s" %% "avro4s-core" % "3.1.0",
  "com.sksamuel.avro4s" %% "avro4s-kafka" % "3.1.0",
  "com.julianpeeters" %% "avrohugger-core" % "1.0.0-RC22",
  "org.apache.kafka" % "kafka-streams-test-utils" % "2.5.0" % Test,
  "org.scalatest" %% "scalatest" % "3.2.0-M4" % Test


)

assemblyMergeStrategy in assembly := {
  case PathList("META-INF", xs @ _*) => MergeStrategy.discard
  case x => MergeStrategy.first
}