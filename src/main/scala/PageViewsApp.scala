import java.time.{Duration, Instant}
import java.util.UUID

import com.sksamuel.avro4s.kafka.GenericSerde
import io.confluent.kafka.streams.serdes.avro.GenericAvroSerde
import org.apache.avro.generic.GenericRecord
import org.apache.kafka.streams.kstream.Suppressed.BufferConfig
import org.apache.kafka.streams.kstream.{JoinWindows, Suppressed}
import org.apache.kafka.streams.scala.{ByteArrayWindowStore, Serdes}
import org.apache.kafka.streams.scala.kstream.{
  Consumed,
  Grouped,
  KStream,
  Materialized
}
import org.apache.kafka.streams.scala.StreamsBuilder

import collection.immutable.ListMap
import model._
import org.apache.kafka.streams.Topology
import org.apache.kafka.streams.kstream.TimeWindows

import scala.util.Try

object PageViewsApp {

  def apply(windowDuration: Duration, advanceBy: Duration): PageViewsApp =
    new PageViewsApp(windowDuration, advanceBy)

  private def mkPageView(r: GenericRecord): Option[PageView] = {
    Try(
      PageView(
        viewtime = r.get("viewtime").asInstanceOf[Long],
        userid = r.get("userid").toString,
        pageid = r.get("pageid").toString
      )).toOption
  }

  private def mkUser(r: GenericRecord): Option[User] = {
    Try(
      User(
        userid = r.get("userid").toString,
        gender = r.get("gender").toString
      )).toOption
  }

  private def mkPageViewResult(pageView: PageView,
                               user: User): PageViewResult = {
    PageViewResult(
      pageView.viewtime,
      pageView.userid,
      pageView.pageid,
      user.gender
    )
  }

  def aggregateData(key: String,
                    pageView: PageViewResult,
                    agg: Aggregate): Aggregate = {
    val pageCounts = agg.topTenPages
    val timesViewed: Long = pageCounts.getOrElse(pageView.pageid, 0L)
    val topTenPages = ListMap(
      (pageCounts + (pageView.pageid -> (timesViewed + 1L))).toSeq
        .sortBy(_._2)(Ordering.Long.reverse)
        .take(10): _*)
    val uniqueUsers = agg.uniqueUsers + pageView.userid
    val totalViewTime = agg.totalViewTime + pageView.viewtime
    Aggregate(uniqueUsers, topTenPages, totalViewTime, pageView.userGender)
  }
}

case class PageViewsApp private (windowDuration: Duration,
                                 windowAdvanceBy: Duration) {

  import PageViewsApp._
  import AppSerdes._
  import org.apache.kafka.streams.scala.Serdes._
  import org.apache.kafka.streams.scala.ImplicitConversions._

  private def getUsersStream(usersTopicName: String, builder: StreamsBuilder)(
      implicit genericRecordSerde: GenericAvroSerde): KStream[String, User] =
    builder
      .stream[String, GenericRecord](usersTopicName)(
        Consumed.`with`(Serdes.String, genericRecordSerde))
      .map { (_, v) =>
        mkUser(v).map(u => (u.userid, u)).getOrElse((null, null))
      }
      .filter((_, v) => v != null)

  private def getPageViewsStream(
      pageViewsTopicName: String,
      builder: StreamsBuilder)(implicit genericRecordSerde: GenericAvroSerde)
    : KStream[String, PageView] =
    builder
      .stream[Long, GenericRecord](pageViewsTopicName)(
        Consumed.`with`(Serdes.Long, genericRecordSerde))
      .map { (_, v) =>
        mkPageView(v).map(pv => (pv.userid, pv)).getOrElse((null, null))
      }
      .filter((_, v) => v != null)

  def createTopology(pageViewsTopicName: String,
                     usersTopicName: String,
                     outputTopic: String,
                     storeName: String)(
      implicit usersSerde: GenericSerde[User],
      pageviewSerde: GenericSerde[PageView],
      genericRecordSerde: GenericAvroSerde): Topology = {

    val builder = new StreamsBuilder
    val users = getUsersStream(usersTopicName, builder)
    val pageViews = getPageViewsStream(pageViewsTopicName, builder)

    val hoppingWindow = TimeWindows
      .of(windowDuration)
      .advanceBy(windowAdvanceBy)
      .grace(Duration.ofSeconds(0))

    pageViews
      .join(users)((left, right) => mkPageViewResult(left, right),
                   JoinWindows.of(windowDuration))
      .groupBy((_, v) => s"${v.viewtime}-${v.userGender}")(
        Grouped.`with`(Serdes.String, pageViewResultSerde))
      .windowedBy(hoppingWindow)
      .aggregate(Aggregate(Set.empty[String], ListMap.empty[String, Long]))(
        aggregateData)(
        Materialized.as[String, Aggregate, ByteArrayWindowStore](storeName))
      .suppress(Suppressed.untilWindowCloses(BufferConfig.unbounded()))
      .toStream((windowedKey, _) => windowedKey.key())
      .flatMap((_, agg) => {
        agg.topTenPages.map {
          case (pageId, _) =>
            val topPage = TopPage(pageId,
                                  agg.gender,
                                  agg.totalViewTime,
                                  agg.uniqueUsers.size)
            (UUID.randomUUID().toString, topPage)
        }
      })
      .to(outputTopic)
    builder.build()
  }

}
