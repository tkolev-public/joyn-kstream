package object model {

  case class PageView(viewtime: Long, userid: String, pageid: String)

  case class PageViewResult(viewtime: Long,
                            userid: String,
                            pageid: String,
                            userGender: String)
  case class User(
      userid: String,
      gender: String
  )

  case class AggregateKey(viewtime: String, gender: String)

  case class Aggregate(uniqueUsers: Set[String],
                       topTenPages: Map[String, Long],
                       totalViewTime: Long = 0L,
                       gender: String = "")

  case class TopPage(pageid: String,
                     gender: String,
                     totalViewTime: Long,
                     numUniqueUsers: Long)

}
