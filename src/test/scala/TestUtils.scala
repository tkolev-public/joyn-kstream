import java.io.File
import java.util
import java.util.Collections

import com.sksamuel.avro4s.BinaryFormat
import com.sksamuel.avro4s.kafka.GenericSerde
import io.confluent.kafka.schemaregistry.avro.AvroSchema
import io.confluent.kafka.schemaregistry.client.MockSchemaRegistryClient
import io.confluent.kafka.streams.serdes.avro.GenericAvroSerde
import model.{PageView, User}
import org.apache.avro.Schema

import model._
import org.apache.avro.generic.{GenericData, GenericRecord}

object TestUtils {

  val MockSchemaRegistryUrl = "mock://mock-schema-url"
  val UsersSchemaPath = "src/test/resources/users-schema.json"
  val PageViewsSchemaPath ="src/test/resources/pageviews-schema.json"

  val usersSchema: Schema = new Schema.Parser()
    .parse(new File(UsersSchemaPath))

  val pageviewSchema: Schema = new Schema.Parser()
    .parse(new File(PageViewsSchemaPath))

  val serdeTestConfig: util.Map[String, String] =
    Collections.singletonMap("schema.registry.url", MockSchemaRegistryUrl)

  implicit val testAvroSerde: GenericAvroSerde = {
    val client: MockSchemaRegistryClient = new MockSchemaRegistryClient()
    val pageviewSchema = new Schema.Parser()
      .parse(new File(PageViewsSchemaPath))
    client.register("pageviews", new AvroSchema(pageviewSchema))

    val serde = new GenericAvroSerde(client)
    serde.configure(serdeTestConfig, false);
    serde
  }

  implicit val userTestSerde: GenericSerde[User] = {
    val userSerde = new GenericSerde[User](BinaryFormat)
    userSerde.configure(serdeTestConfig, false)
    userSerde
  }

  implicit val pageViewSerde: GenericSerde[PageView] = {
    val pageViewSerde = new GenericSerde[PageView](BinaryFormat)
    pageViewSerde.configure(serdeTestConfig, false)
    pageViewSerde
  }

  val topPageSerde: GenericSerde[TopPage] =
    new GenericSerde[TopPage](BinaryFormat)

  implicit def userToRecord(user: User): GenericRecord = {

    val userRecord = new GenericData.Record(usersSchema)
    userRecord.put("userid", user.userid)
    userRecord.put("gender", user.gender)
    userRecord
  }

  implicit def pageViewToRecord(pv: PageView): GenericRecord = {

    val pageviewRecord1 = new GenericData.Record(pageviewSchema)
    pageviewRecord1.put("viewtime", pv.viewtime)
    pageviewRecord1.put("userid", pv.userid)
    pageviewRecord1.put("pageid", pv.pageid)
    pageviewRecord1
  }
}
